﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace memoryGame
{
    public partial class Form1 : Form
    {
        public PictureBox p,p1, p2;//global
        Random rnd = new Random();
        PictureBox[] allp;
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            
        }
        private void Shuffle()//מערבב תמונות
        {
            int num;
            PictureBox tmp;
            for (int i = 0; i < allp.Length; i++)
            {
                num = rnd.Next(0, allp.Length);
                tmp = allp[i];
                allp[i] = allp[num];
                allp[num] = tmp;
            }
        }
        private void ShowBoard(int row,int col)
        {
            int x = 100, y = 100,place=0;
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    allp[place].Image= (Image)Properties.Resources.ResourceManager.GetObject("picBackground");
                    allp[place].Location = new Point(x, y);
                    x += 105;
                    Controls.Add(allp[place++]);
                }
                x = 100;
                y += 105;
            }
        }
        private void BuildPicture(int size)
        {
            int picNum = 1;
            allp = new PictureBox[size];
            for (int i = 0; i < allp.Length-1; i+=2)
            {
                allp[i] = new PictureBox();
                allp[i].Image = (Image)Properties.Resources.ResourceManager.GetObject("pic" + (picNum));
                allp[i].Size = new Size(100, 100);
                allp[i].SizeMode = PictureBoxSizeMode.StretchImage;
                allp[i].BorderStyle = BorderStyle.FixedSingle;
                allp[i].Enabled = true;
                allp[i].Tag = picNum + "";
                allp[i].Click += allP_Click;

                allp[i + 1] = new PictureBox();
                allp[i + 1].Tag = picNum + "";
                allp[i + 1].Image = (Image)Properties.Resources.ResourceManager.GetObject("pic" + (picNum));
                picNum++;
                allp[i + 1].Enabled = true;
                allp[i + 1].Size = new Size(100, 100);
                allp[i + 1].SizeMode = PictureBoxSizeMode.StretchImage;
                allp[i + 1].BorderStyle = BorderStyle.FixedSingle;                             
                allp[i + 1].Click += allP_Click;//כשלוחצים על תמונה אז נפתחת פעולה
            }
        }      
        int count1 = 1;
        private void timeSwitch_Tick(object sender, EventArgs e)
        {          
            count1--;
            if(count1==0)
            {
                p1.Image = (Image)Properties.Resources.ResourceManager.GetObject("picBackground");
                p2.Image = (Image)Properties.Resources.ResourceManager.GetObject("picBackground");
                timeSwitch.Enabled = false;
                count1 = 1;
            }           
        }
        int counttie = 0;
        int count =0;
        private void lblScore_Click(object sender, EventArgs e)
        {

        }
        private void label1_Click(object sender, EventArgs e)
        {

        }       
        private void lblStart_Click(object sender, EventArgs e)
        {
            if(txtPlayer1.Text!= txtPlayer2.Text && txtPlayer1.Text!="" && txtPlayer2.Text!="")
            {
                count = 0;
                score1 = 0;
                score2 = 0;
                #region View
                lblTitle.Visible = false;
                lblStart.Visible = false;
                picWorld.Visible = false;               
                lblTurn.Visible = true;
                lblPrint.Visible = true;
                lblScore1.Visible = true;
                lblScore2.Visible = true;
                lblPrintScore1.Visible = true;
                lblPrintScore2.Visible = true;
                lblWin.Visible = false;
                lblPicWin.Visible = false;
                lblPlayer1.Visible = false;
                lblPlayer2.Visible = false;
                txtPlayer1.Visible = false;
                txtPlayer2.Visible = false;
                #endregion
                lblScore1.Text = "Score of " + txtPlayer1.Text + ":";
                lblScore2.Text = "Score of " + txtPlayer2.Text + ":";
                lblPrint.Text = txtPlayer1.Text;
                pl1 = txtPlayer1.Text;
                pl2 = txtPlayer2.Text;
                turnof = pl1;
                BuildPicture(40);
                Shuffle();
                ShowBoard(5, 8);
            }    
            else if(txtPlayer1.Text == "" || txtPlayer2.Text == "")
            {
                MessageBox.Show("one name is empy or both");
            }     
        }
        string pl1;
        string pl2;
        string turnof;
        int score1 = 0, score2 = 0;
        private void lblNext_Click(object sender, EventArgs e)
        {
            if (txtPlayer1.Text != txtPlayer2.Text && txtPlayer1.Text != "" && txtPlayer2.Text != "")
            {
                #region View
                lblPlayer1.Visible = false;
                lblPlayer2.Visible = false;
                txtPlayer1.Visible = false;
                txtPlayer2.Visible = false;
                lblTurn.Visible = true;
                lblPrint.Visible = true;
                lblScore1.Visible = true;
                lblScore2.Visible = true;
                lblPrintScore1.Text = "0";
                lblPrintScore2.Text = "0";
                lblPrintScore1.Visible = true;
                lblPrintScore2.Visible = true;
                lblNext.Visible = false;
                #endregion
                lblScore1.Text = "Score of " + txtPlayer1.Text + ":";
                lblScore2.Text = "Score of " + txtPlayer2.Text + ":";
                lblPrint.Text = txtPlayer1.Text;
                pl1 = txtPlayer1.Text;
                pl2 = txtPlayer2.Text;
                turnof = pl1;
                for (int i = 0; i < allp.Length; i++)
                {
                    allp[i].Enabled = true;
                    allp[i].Visible = true;
                }
                Shuffle();
                ShowBoard(5, 8);
            }
            else if (txtPlayer1.Text == "" || txtPlayer2.Text == "")
            {
                MessageBox.Show("one name is empy or both");
            }
        }
        private void lblAgain_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < allp.Length; i++)
            {
                allp[i].Visible = false;                                                                             
            }
            count = 0;
            score1 = 0;
            score2 = 0;
            #region View
            lblPlayer1.Visible = true;
            lblPlayer2.Visible = true;
            lblAgain.Visible = false;           
            txtPlayer1.Visible = true;
            txtPlayer2.Visible = true;
            lblTurn.Visible = false;
            lblPrint.Visible = false;
            lblScore1.Visible = false;
            lblScore2.Visible = false;
            lblPrintScore1.Visible = false;
            lblPrintScore2.Visible = false;
            lblWin.Visible = false;
            lblPicWin.Visible = false;            
            lblNext.Visible = true;
            #endregion
        }
        private void label3_Click(object sender, EventArgs e)
        {

        }
        PictureBox check;
        private void allP_Click(object sender, EventArgs e)
        {         
           check = (PictureBox)sender;            
            if (timeSwitch.Enabled == false&&(check.Enabled==true))
            {
                count++;
                if (count == 1)
                {
                    p1 = (PictureBox)sender;
                    p1.Image = (Image)Properties.Resources.ResourceManager.GetObject("pic" + (p1.Tag));
                    p1.Enabled = false;
                }
                if (count == 2)
                {
                    p2 = (PictureBox)sender;
                    p2.Image = (Image)Properties.Resources.ResourceManager.GetObject("pic" + (p2.Tag));
                    timeSwitch.Enabled = true;
                    p2.Enabled = false;
                    if (p2.Tag.Equals(p1.Tag))
                    {
                        timeSwitch.Enabled = false;
                        if (turnof.Equals(txtPlayer1.Text))
                        {
                            score1++;
                            lblPrintScore1.Text = score1 + "";
                        }
                        else
                        {
                            score2++;
                            lblPrintScore2.Text = score2 + "";
                        }
                    }
                    else
                    {
                        p1.Enabled = true;
                        p2.Enabled = true;
                    }
                    if (turnof.Equals(txtPlayer1.Text))
                    {
                        turnof = txtPlayer2.Text;
                        lblPrint.Text = turnof;
                    }
                    else
                    {
                        turnof = txtPlayer1.Text;
                        lblPrint.Text = turnof;
                        count = 0;
                    }
                    count = 0;
                }
                if (score1 + score2 == 20)
                {
                    lblPicWin.Visible = true;
                    lblWin.Visible = true;
                    if (score1 > score2)
                        lblWin.Text = "The Winner is:" + txtPlayer1.Text;
                    if (score1 < score2)
                        lblWin.Text = "The Winner is:" + txtPlayer2.Text;
                    if (score1 == score2)
                        lblWin.Text = "TIE!!!";
                    lblAgain.Visible = true;
                }
            }                
        }
    }
}