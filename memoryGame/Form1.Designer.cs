﻿namespace memoryGame
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblScore1 = new System.Windows.Forms.Label();
            this.lblScore2 = new System.Windows.Forms.Label();
            this.timeSwitch = new System.Windows.Forms.Timer(this.components);
            this.lblStart = new System.Windows.Forms.Label();
            this.lblPlayer1 = new System.Windows.Forms.Label();
            this.lblPlayer2 = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblTurn = new System.Windows.Forms.Label();
            this.lblPrint = new System.Windows.Forms.Label();
            this.txtPlayer1 = new System.Windows.Forms.TextBox();
            this.txtPlayer2 = new System.Windows.Forms.TextBox();
            this.lblPrintScore1 = new System.Windows.Forms.Label();
            this.lblPrintScore2 = new System.Windows.Forms.Label();
            this.lblWin = new System.Windows.Forms.Label();
            this.lblNext = new System.Windows.Forms.Label();
            this.lblAgain = new System.Windows.Forms.Label();
            this.lblPicWin = new System.Windows.Forms.Label();
            this.picWorld = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picWorld)).BeginInit();
            this.SuspendLayout();
            // 
            // lblScore1
            // 
            this.lblScore1.AutoSize = true;
            this.lblScore1.Font = new System.Drawing.Font("Andalus", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScore1.Location = new System.Drawing.Point(954, 274);
            this.lblScore1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblScore1.Name = "lblScore1";
            this.lblScore1.Size = new System.Drawing.Size(155, 57);
            this.lblScore1.TabIndex = 0;
            this.lblScore1.Text = "Score of:";
            this.lblScore1.Visible = false;
            this.lblScore1.Click += new System.EventHandler(this.lblScore_Click);
            // 
            // lblScore2
            // 
            this.lblScore2.AutoSize = true;
            this.lblScore2.Font = new System.Drawing.Font("Andalus", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScore2.Location = new System.Drawing.Point(954, 367);
            this.lblScore2.Name = "lblScore2";
            this.lblScore2.Size = new System.Drawing.Size(155, 57);
            this.lblScore2.TabIndex = 1;
            this.lblScore2.Text = "Score of:";
            this.lblScore2.Visible = false;
            // 
            // timeSwitch
            // 
            this.timeSwitch.Interval = 700;
            this.timeSwitch.Tick += new System.EventHandler(this.timeSwitch_Tick);
            // 
            // lblStart
            // 
            this.lblStart.AutoSize = true;
            this.lblStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblStart.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblStart.Font = new System.Drawing.Font("Algerian", 60F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStart.Location = new System.Drawing.Point(398, 562);
            this.lblStart.Name = "lblStart";
            this.lblStart.Size = new System.Drawing.Size(288, 91);
            this.lblStart.TabIndex = 4;
            this.lblStart.Text = "START";
            this.lblStart.Click += new System.EventHandler(this.lblStart_Click);
            // 
            // lblPlayer1
            // 
            this.lblPlayer1.AutoSize = true;
            this.lblPlayer1.Font = new System.Drawing.Font("Andalus", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayer1.Location = new System.Drawing.Point(318, 216);
            this.lblPlayer1.Name = "lblPlayer1";
            this.lblPlayer1.Size = new System.Drawing.Size(212, 73);
            this.lblPlayer1.TabIndex = 5;
            this.lblPlayer1.Text = "Player 1:";
            // 
            // lblPlayer2
            // 
            this.lblPlayer2.AutoSize = true;
            this.lblPlayer2.Font = new System.Drawing.Font("Andalus", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayer2.Location = new System.Drawing.Point(318, 351);
            this.lblPlayer2.Name = "lblPlayer2";
            this.lblPlayer2.Size = new System.Drawing.Size(212, 73);
            this.lblPlayer2.TabIndex = 6;
            this.lblPlayer2.Text = "Player 2:";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Andalus", 60F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(278, 9);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(545, 122);
            this.lblTitle.TabIndex = 7;
            this.lblTitle.Text = "Memory Game";
            // 
            // lblTurn
            // 
            this.lblTurn.AutoSize = true;
            this.lblTurn.Font = new System.Drawing.Font("Andalus", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTurn.Location = new System.Drawing.Point(951, 98);
            this.lblTurn.Name = "lblTurn";
            this.lblTurn.Size = new System.Drawing.Size(202, 73);
            this.lblTurn.TabIndex = 8;
            this.lblTurn.Text = "Turn Of:";
            this.lblTurn.Visible = false;
            this.lblTurn.Click += new System.EventHandler(this.label3_Click);
            // 
            // lblPrint
            // 
            this.lblPrint.AutoSize = true;
            this.lblPrint.Font = new System.Drawing.Font("Andalus", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrint.Location = new System.Drawing.Point(1159, 98);
            this.lblPrint.Name = "lblPrint";
            this.lblPrint.Size = new System.Drawing.Size(60, 73);
            this.lblPrint.TabIndex = 9;
            this.lblPrint.Text = "0";
            this.lblPrint.Visible = false;
            // 
            // txtPlayer1
            // 
            this.txtPlayer1.Font = new System.Drawing.Font("Andalus", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlayer1.Location = new System.Drawing.Point(596, 227);
            this.txtPlayer1.Name = "txtPlayer1";
            this.txtPlayer1.Size = new System.Drawing.Size(145, 81);
            this.txtPlayer1.TabIndex = 10;
            // 
            // txtPlayer2
            // 
            this.txtPlayer2.Font = new System.Drawing.Font("Andalus", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlayer2.Location = new System.Drawing.Point(596, 341);
            this.txtPlayer2.Name = "txtPlayer2";
            this.txtPlayer2.Size = new System.Drawing.Size(145, 81);
            this.txtPlayer2.TabIndex = 11;
            // 
            // lblPrintScore1
            // 
            this.lblPrintScore1.AutoSize = true;
            this.lblPrintScore1.Font = new System.Drawing.Font("Andalus", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrintScore1.Location = new System.Drawing.Point(1213, 274);
            this.lblPrintScore1.Name = "lblPrintScore1";
            this.lblPrintScore1.Size = new System.Drawing.Size(45, 57);
            this.lblPrintScore1.TabIndex = 12;
            this.lblPrintScore1.Text = "0";
            this.lblPrintScore1.Visible = false;
            // 
            // lblPrintScore2
            // 
            this.lblPrintScore2.AutoSize = true;
            this.lblPrintScore2.Font = new System.Drawing.Font("Andalus", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrintScore2.Location = new System.Drawing.Point(1213, 363);
            this.lblPrintScore2.Name = "lblPrintScore2";
            this.lblPrintScore2.Size = new System.Drawing.Size(45, 57);
            this.lblPrintScore2.TabIndex = 13;
            this.lblPrintScore2.Text = "0";
            this.lblPrintScore2.Visible = false;
            // 
            // lblWin
            // 
            this.lblWin.AutoSize = true;
            this.lblWin.Font = new System.Drawing.Font("Andalus", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWin.ForeColor = System.Drawing.Color.Red;
            this.lblWin.Location = new System.Drawing.Point(405, 696);
            this.lblWin.Name = "lblWin";
            this.lblWin.Size = new System.Drawing.Size(77, 98);
            this.lblWin.TabIndex = 14;
            this.lblWin.Text = "0";
            this.lblWin.Visible = false;
            // 
            // lblNext
            // 
            this.lblNext.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblNext.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNext.Font = new System.Drawing.Font("Algerian", 60F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNext.Location = new System.Drawing.Point(398, 501);
            this.lblNext.Name = "lblNext";
            this.lblNext.Size = new System.Drawing.Size(288, 91);
            this.lblNext.TabIndex = 17;
            this.lblNext.Text = "NEXT";
            this.lblNext.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblNext.Visible = false;
            this.lblNext.Click += new System.EventHandler(this.lblNext_Click);
            // 
            // lblAgain
            // 
            this.lblAgain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblAgain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAgain.Font = new System.Drawing.Font("Algerian", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAgain.Location = new System.Drawing.Point(1003, 528);
            this.lblAgain.Name = "lblAgain";
            this.lblAgain.Size = new System.Drawing.Size(267, 153);
            this.lblAgain.TabIndex = 18;
            this.lblAgain.Text = "PLAY      AGAIN    ";
            this.lblAgain.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblAgain.Visible = false;
            this.lblAgain.Click += new System.EventHandler(this.lblAgain_Click);
            // 
            // lblPicWin
            // 
            this.lblPicWin.Image = global::memoryGame.Properties.Resources.gameOver;
            this.lblPicWin.Location = new System.Drawing.Point(39, 87);
            this.lblPicWin.Name = "lblPicWin";
            this.lblPicWin.Size = new System.Drawing.Size(921, 625);
            this.lblPicWin.TabIndex = 15;
            this.lblPicWin.Visible = false;
            // 
            // picWorld
            // 
            this.picWorld.Image = global::memoryGame.Properties.Resources.world;
            this.picWorld.Location = new System.Drawing.Point(860, 227);
            this.picWorld.Name = "picWorld";
            this.picWorld.Size = new System.Drawing.Size(435, 286);
            this.picWorld.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picWorld.TabIndex = 19;
            this.picWorld.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1322, 803);
            this.Controls.Add(this.picWorld);
            this.Controls.Add(this.lblAgain);
            this.Controls.Add(this.lblNext);
            this.Controls.Add(this.lblPicWin);
            this.Controls.Add(this.lblWin);
            this.Controls.Add(this.lblPrintScore2);
            this.Controls.Add(this.lblPrintScore1);
            this.Controls.Add(this.txtPlayer2);
            this.Controls.Add(this.txtPlayer1);
            this.Controls.Add(this.lblPrint);
            this.Controls.Add(this.lblTurn);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.lblPlayer2);
            this.Controls.Add(this.lblPlayer1);
            this.Controls.Add(this.lblStart);
            this.Controls.Add(this.lblScore2);
            this.Controls.Add(this.lblScore1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picWorld)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblScore1;
        private System.Windows.Forms.Label lblScore2;
        private System.Windows.Forms.Timer timeSwitch;
        private System.Windows.Forms.Label lblStart;
        private System.Windows.Forms.Label lblPlayer1;
        private System.Windows.Forms.Label lblPlayer2;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblTurn;
        private System.Windows.Forms.Label lblPrint;
        private System.Windows.Forms.TextBox txtPlayer1;
        private System.Windows.Forms.TextBox txtPlayer2;
        private System.Windows.Forms.Label lblPrintScore1;
        private System.Windows.Forms.Label lblPrintScore2;
        private System.Windows.Forms.Label lblWin;
        private System.Windows.Forms.Label lblPicWin;
        private System.Windows.Forms.Label lblNext;
        private System.Windows.Forms.Label lblAgain;
        private System.Windows.Forms.PictureBox picWorld;
    }
}

